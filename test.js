var Hash = require('./main');

console.log('random:', Hash.random(9));

console.log('md5:', Hash.md5('hello'));
console.log('sha1:', Hash.sha1('hello'));
console.log('sha256:', Hash.sha256('hello'));
console.log('sha512:', Hash.sha512('hello'));
console.log('pwd:', Hash.pwd('hello'));

console.log('genId:', Hash.genId('id-1','id-2'));
console.log('---------------with password-------------------');
console.log('md5 + pwd:', Hash.md5('hello', 'pwd'));
console.log('sha1 + pwd:', Hash.sha1('hello', 'pwd'));
console.log('sha256 + pwd:', Hash.sha256('hello', 'pwd'));
console.log('sha512 + pwd:', Hash.sha512('hello', 'pwd'));

console.log('---------------encrypt-------------------');

var e = Hash.encrypt('hello', 'pwd');

console.log('encrypt:', e);
console.log('decrypt:', Hash.decrypt(e, 'pwd'));




